# Assignment 2 Self-Assessment Instructions

There are two Selenium test cases to help you test you test your CGI
application.  [Tests for the Calculator][calculator-test] and
[Tests for the Guestbook][guestbook-test]. Note that these tests only
check that the proper forms and elements are on your page.  But,
assuming that your URLs are correct, this is enough to ensure that the
grading scripts will be able to correctly interact with your
application.

Here are the instructions to run the test cases:

- Install [Selenium IDE][selenium-ide] in Firefox
- Open the HTML page of your assignment
- Open Selenium IDE in Firefox
- Click Menu File -> Open -> Select the provided test script in .html
- Click the icon "Play current test case"
- If the output is all green then your HTML page has passed the test

The selenium tests are meant to verify the elements of required
attribute names and tags are present. Please make sure that your HTML
pages pass the test case before submission.

[selenium-ide]: http://www.seleniumhq.org/download/
[guestbook-test]: https://gitlab.com/utk-web-security/public-s16/raw/master/assignment-2/self_assessment/GuestbookClientTest.html
[calculator-test]: https://gitlab.com/utk-web-security/public-s16/raw/master/assignment-2/self_assessment/CalculatorClientTest.html
