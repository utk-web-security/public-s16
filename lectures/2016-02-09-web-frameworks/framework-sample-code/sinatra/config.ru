require 'data_mapper'
require './models/user.rb'
require './app.rb'

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, "sqlite://#{Dir.pwd}/development.db")
DataMapper.finalize
DataMapper.auto_upgrade!
# DataMapper.auto_migrate!

run UserManagement
