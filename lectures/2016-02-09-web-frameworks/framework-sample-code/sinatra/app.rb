require 'sinatra'
require 'data_mapper' # ORM for models

class UserManagement < Sinatra::Base
  get '/users' do
    erb :'users/index', locals: { users: User.all }
  end

  post '/users' do
    user = User.create(name: params[:name], occupation: params[:occupation])
    redirect "/users/#{user.id}"
  end

  get %r{\A/users/(\d+)\z} do |id|
    user = User.get(id)
    erb :'/users/show', locals: { user: user }
  end

  get '/users/new' do
    erb :'users/new'
  end

  delete %r{\A/users/(\d+)\z} do |id|
    id = User.get(id)
    user.destroy
    redirect '/users'
  end

  get '/hello' do
    'Hello world!'
  end
end
