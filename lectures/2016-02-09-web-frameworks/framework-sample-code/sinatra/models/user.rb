class User
  include DataMapper::Resource

  property :id, Serial
  property :name, String, required: true
  property :occupation, String, default: 'engineer'
  property :created_at, DateTime
end
